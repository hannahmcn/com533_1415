	<?php
	require	'Slim/Slim.php';	
\Slim\Slim::registerAutoloader();	
	use	Slim\Slim;	
$app	=	new	Slim();
$app->get('/messages',	'getMessages');	// rest with php:helloworld
$app->get('/movies',	'getMovies');	//Rest Get	
$app->post('/movies',	'addMovie');  //Rest POST
$app->get('/movies/:id',		'getMovie');	//Rest Get single record
$app->put('/movies/:id',	'updateMovie');	
$app->delete('/movies/:id',			'deleteMovie');	


$app->run();	

//hello world
function	getMessages()	{	
				echo	"Hello	World!";	
}	


//REST GET

function	getMovies()	{	
				$sql	=	"select	*	FROM	movie	ORDER	BY	Movie_Id	";	
				try	{	
								$db	=	getConnection();	
								$stmt	=	$db->query($sql);	
								$movies	=	$stmt->fetchAll(PDO::FETCH_OBJ);	
								$db	=	null;	
								responseJson('{"movie":	' 	.json_encode($movies)	.	'}',200);	
					} catch(PDOException	$e)	{	
		 						responseJson(	'{"error":{"text":'.	$e->getMessage()	.'}}',	500);	

}	
}	


//REST GET - single record

function	getMovie($id)	{	
				$sql	=	"SELECT	*	FROM	movie	WHERE	movie_id=:id";	
				try	{	
								$db	=	getConnection();	
								$stmt	=	$db->prepare($sql);	
								$stmt->bindParam("id",	$id);	
								$stmt->execute();	
								$staff	=	$stmt->fetchObject();	
								$db	=	null;	
								responseJson(json_encode($staff),200);	
				}	catch(PDOException	$e)	{	
		 						responseJson(	'{"error":{"text":'.	$e->getMessage()	.'}}',	500);	

}	
}	

//REST POST

function	addMovie()	{	
				$request	=	Slim::getInstance()->request();	
				$movie	=	json_decode($request->getBody());	
				$sql	=	"insert	into	movie	(movie_name,	movie_genre,movie_description,movie_author,release_date)	values	(:movie_name, :movie_genre,	:movie_description,	:movie_author, :release_date)";	
				try	{
								$db	=	getConnection();	
								$stmt	=	$db->prepare($sql);	
								$stmt->bindParam("movie_name",	$movie->movie_name);	
								$stmt->bindParam("movie_genre",	$movie->movie_genre);	
								$stmt->bindParam("movie_description",	$movie->movie_description);	
								$stmt->bindParam("movie_author",$movie->movie_author);	
								$stmt->bindParam("release_date",$movie->release_date);	
								$stmt->execute();	
								$movie->Movie_Id	=	$db->lastInsertId();	
								$db	=	null;	
								responseJson(json_encode($movie),201);	
				}	catch(PDOException	$e)	{	
								responseJson(	'{"error":{"text":'.	$e->getMessage()	.'}}',500);	
}	
}	



//REST PUT

function	updateMovie($id)	{	
				$request	=	Slim::getInstance()->request();	
				$body	=	$request->getBody();	
				$movie	=	json_decode($body);	
				$sql	=	"UPDATE	movie	SET	movie_name=:movie_name,	movie_genre=:movie_genre,	
movie_description=:movie_description, movie_author=:movie_author, release_date=:release_date	WHERE	movie_id=:id";	
					try	{	
						 
								$movie	=	$stmt->fetchObject();	
								$db	=	null;	
								responseJson(json_encode($movie),200);	
				}	catch(PDOException	$e)	{	
		 						responseJson(	'{"error":{"text":'.	$e->getMessage()	.'}}',	500);	

}	
}	

//REST DELETE

function	deleteMovie($id)	{	
				$sql	=	"DELETE	FROM	movie	WHERE	movie_id=:id";	
					try	{	
						 	
								$movie	=	$stmt->fetchObject();	
								$db	=	null;	
								responseJson(json_encode($movie),200);	
				}	catch(PDOException	$e)	{	
		 						responseJson(	'{"error":{"text":'.	$e->getMessage()	.'}}',	500);	

}	
}	


function getConnection() {
                $dbhost="localhost";
                $dbuser="B00563792";
                $dbpass="QaxxAF4U";
                $dbname="b00563792";
                $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
	
				$dbh->setAttribute(PDO::ATTR_ERRMODE,	PDO::ERRMODE_EXCEPTION);	
				return	$dbh;	
}	



function	responseJson($responseBody,	$statusCode){	
				 $app	=	Slim::getInstance();	
					 $response	=	$app->response();	
$response['Content-Type']	=	'application/json';	
$response->status($statusCode);	
$response->body($responseBody);	
}

?>	