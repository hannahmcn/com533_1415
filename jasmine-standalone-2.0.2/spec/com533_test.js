describe("(0+1+...+n)/n",	function()	{	
		 it("should	return	2if	n	is	3",	function()	{	
				 var actualReturned=calc_func(3);	
				 expect(actualReturned).toEqual(2);	
		 });	
		 it("should	return	'Value	not	allowed'	if	n	is	0",	function()	{	
				 var actualReturned=calc_func(0);	
				 expect(actualReturned).toEqual('Value	not	allowed');	
		 });	
});